package org.example;

public class CompteEpargne extends Compte  {


    private float taux;
    private final int POURCENTAGE = 100;


    public CompteEpargne(float solde, float taux) {
        super(solde);
        this.taux = taux;
        creationInfo();
    }


    public void calculInteret() {
        this.solde = this.solde * (1 + (taux / POURCENTAGE));
    }

    public float getTaux() {
        return taux;
    }

    public void setTaux(float taux) {
        this.taux = taux;
    }


    @Override
    public void quelleCompte() {
        System.out.println("Je suis un compte Epargne");
    }

    @Override
    public String toString() {
        return "CompteEpargne{" +
                "taux=" + taux +
                ", POURCENTAGE=" + POURCENTAGE +
                ", solde=" + solde +
                "} " + super.toString();
    }


    @Override
    public void creationInfo() {
        System.out.println("Je viens d'etre crée et je suis un compte Epargne");

    }
}
