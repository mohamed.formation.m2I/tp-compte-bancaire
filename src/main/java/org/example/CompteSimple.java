package org.example;

public class CompteSimple extends Compte{

    private float decouvert;


    public CompteSimple(float solde, float decouvert) {
        super(solde);
        this.decouvert = decouvert;

    }


    @Override
    public void quelleCompte() {
        System.out.println("Je suis un compte simple");
    }

    public void retirer(float mnt){
        if(this.solde + this.decouvert> mnt){
            solde -=mnt;
        }
    }

    public float getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(float decouvert) {
        this.decouvert = decouvert;
    }

    @Override
    public String toString() {
        return "CompteSimple{" +
                "decouvert=" + decouvert +
                "} " + super.toString();
    }

    @Override
    public void creationInfo() {
        System.out.println("Je viens d'etre crée et je suis un compte Payant");
    }


}
