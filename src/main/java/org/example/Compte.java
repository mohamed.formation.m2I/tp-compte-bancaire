package org.example;

public abstract class Compte implements Creation  {

    private int code;

    protected float solde;

    private static int nbComptes = 0;


    public Compte(float solde) {
        this.code = ++nbComptes;
        this.solde = solde;
    }

    public abstract void quelleCompte();

    public Compte() {
        this.code = ++nbComptes;
    }


    public int getCode() {
        return code;
    }


    public float getSolde() {
        return solde;
    }

    public void setSolde(float solde) {
        this.solde = solde;
    }


    public void retirer(float mnt) {
        System.out.println("coucou");
        if(this.solde>mnt){
            this.solde -= mnt;
        }
    }


    public void verser(float mnt) {
        this.solde += mnt;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "code=" + code +
                ", solde=" + solde +
                '}';
    }
}
