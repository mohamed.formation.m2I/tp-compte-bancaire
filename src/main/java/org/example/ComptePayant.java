package org.example;

public class ComptePayant extends Compte {


    private final float TAUX = 5;

    private final float POURCENTAGE = 100;

    public ComptePayant(float solde) {
        super(solde);
    }

    @Override
    public void quelleCompte() {
        System.out.println("Je suis un compte payant");
    }

    @Override
    public void verser(float mnt) {
       super.verser(mnt);
       super.retirer(mnt*(TAUX/POURCENTAGE));
    }

    public void retirer(float mnt) {
        System.out.println("je retire payant");
        super.retirer(mnt);
        super.retirer(mnt * (TAUX / POURCENTAGE));
    }

    @Override
    public String toString() {
        return "ComptePayant{" +
                "solde=" + solde +
                "} " + super.toString();
    }


    @Override
    public void creationInfo() {
        System.out.println("Je viens d'etre crée et je suis un compte Payant");
    }
}
