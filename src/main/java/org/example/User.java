package org.example;

import java.util.ArrayList;
import java.util.List;

public class User {


    private int id;
    private static int count = 0;
    private String nom;
    private String prenom;

    private static List<String> noms = new ArrayList<>();
    private int age;
    private Adresse adresse;
    private List<Compte>comptes = new ArrayList<>();

    public User(String nom, String prenom, int age, Adresse adresse,List<Compte> comptes) {
        this.id = ++count;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.comptes = comptes;
        this.adresse = adresse;
        System.out.println(this.id);
    }

    public User(){
        this.id = ++count;

    }


    public static void hello(){
        System.out.println("Bonjour à tous");
    }


    public void hello2(){
        System.out.println("Rebonjour");
    }

    public static int returnCount(){
        return count;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Compte> getComptes() {
        return comptes;
    }

    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    @Override
    public String toString() {
        return "User{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", age=" + age +
                ", adresse=" + adresse +
                ", comptes=" + comptes +
                '}';
    }
}
